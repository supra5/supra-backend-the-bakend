<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CardToken extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'first_6digits',
        'last_4digits',
        'token',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
