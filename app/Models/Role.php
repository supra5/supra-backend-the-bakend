<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function merchants()
    {
        return $this->hasMany(User::class)->where('role_id', 2);
    }

    public function clients()
    {
        return $this->hasMany(User::class)->where('role_id', 3);
    }
}
