<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'reference',
        'biller_type',
        'biller_name',
        'amount',
        'time_of_purchase',
        'pin',
        'customer',
        'status',
        'network'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
