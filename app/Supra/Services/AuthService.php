<?php

namespace Supra\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthService implements AuthServiceInterface
{
    public function login($email = "", $password = "")
    {
        $invalidMessage = "Invalid credentials";
        $user = User::whereEmail($email)->first();
        if (!$user) {
            throw new \Exception($invalidMessage);
        }
        if (!Hash::check($password, $user->password)) {
            throw new \Exception($invalidMessage);
        }

        $token = $user->createToken($user->role->fullName . " login token");
        return ['token' => $token->plainTextToken, 'user' => $user];
    }

    public function logout($request)
    {
        $request->user()->currentAccessToken()->delete();
        return true;
    }

    public function register($data)
    {
    }

    public function user()
    {
        return auth('sanctum')->user();
    }
}
