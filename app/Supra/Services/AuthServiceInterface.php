<?php

namespace Supra\Services;


interface AuthServiceInterface
{
    public function login($email, $password);

    public function register($data);

    public function logout($request);

    public function user();
}
