<?php

namespace Supra\Services\Payment;

interface BillingServiceInterface
{

    public function buyAirtime(
        $amount,
        $customer,
        $reference,
        $country
    );

    public function buyData(
        $type,
        $amount,
        $customer,
        $reference,
        $country = 'NG'
    );

    public function buyPower(
        $type,
        $amount,
        $meter_number,
        $reference,
        $country = 'NG'
    );

    public function buyCable(
        $type,
        $amount,
        $smartcard_number,
        $reference,
        $country = 'NG'
    );

    public function buyWaec(
        $pinValue,
        $numberOfPins,
        $reference
    );

    public function verifyPaymentAndBuyAirtime(
        $transaction_id
    );

    public function verifyPaymentAndBuyData(
        $transaction_id
    );

    public function verifyPaymentAndBuyCable(
        $transaction_id
    );

    public function verifyPaymentAndBuyPower(
        $transaction_id
    );

    public function verifyPaymentAndBuyWaec($ransaction_id);

    public function verifyPaymentAndUpgradeUser($transaction_id);

    public function initializeDataPayment(
        $amount,
        $billerName,
        $phoneNumber,
        $useCard,
        $saveCard,
        $merchantLink = null
    );

    public function initializeCablePayment(
        $amount,
        $billerName,
        $smartcard_number,
        $useCard,
        $saveCard,
        $merchantLink = null
    );

    public function initializePowerPayment(
        $amount,
        $billerName,
        $meter_number,
        $useCard,
        $saveCard,
        $merchantLink = null
    );

    public function initializeAirtimePayment(
        $amount,
        $phoneNumber,
        $useCard,
        $saveCard,
        $merchantLink = null
    );

    public function initializeWaecPayment(
        $pinValue,
        $numberOfPins,
        $pinDescription,
        $useCard,
        $saveCard,
        $merchantLink = null
    );



    public function initializeUpgradePayment();
}
