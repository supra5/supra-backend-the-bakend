<?php

namespace Supra\Services\Payment;

interface FlutterwaveInterface
{
    public function chargeCard(
        $amount,
        $paymentOption,
        $redirect_url,
        $customer,
        $meta,
        $customizations = []
    );

    public function verifyTransaction($transaction_id);

    public function createBillPayment(
        $country,
        $customer,
        $amount,
        $type,
        $reference
    );

    public function validateBillService(
        $item_code,
        $biller_code,
        $customer
    );

    public function verifyAccountNumber($accountNumber, $accountBank);

    public function transfer($accountNumber, $accountBank, $amount, $reference, $narration, $currency = "NGN");

    public function tokenizedCharge(
        $cardToken,
        $amount,
        $tx_ref,
        $currency,
        $email
    );
}
