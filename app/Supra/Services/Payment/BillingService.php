<?php

namespace Supra\Services\Payment;

use App\Events\BillingPurchased;
use App\Events\MerchantLinkUsed;
use App\Models\Biller;
use App\Models\CardToken;
use App\Models\Earning;
use App\Models\Role;
use App\Models\Setting;
use App\Models\Transaction;
use Illuminate\Http\Client\RequestException;
use Supra\Services\Payment\FlutterwaveInterface;
use App\Models\User;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Supra\Services\Payment\BaxipayInterface;

class BillingService implements BillingServiceInterface
{
    private $flutterWave;
    private $baxipay;
    public function __construct(FlutterwaveInterface $flutterWave, BaxipayInterface $baxipay)
    {
        $this->flutterWave = $flutterWave;
        $this->baxipay = $baxipay;
    }

    public  function buyAirtime($amount, $customer, $reference, $country = 'NG')
    {

        try {
            $response = $this->flutterWave->createBillPayment(
                $country,
                $customer,
                $amount,
                "AIRTIME",
                $reference
            );
            return $response;
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function buyData($type, $amount, $customer,  $reference, $country = 'NG')
    {
        try {
            $response = $this->flutterWave->createBillPayment(
                $country,
                $customer,
                $amount,
                $type,
                $reference
            );
            return $response;
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function verifyPaymentAndBuyCable($transaction_id)
    {
        $data = $this->flutterWave->verifyTransaction($transaction_id);

        $response =  $this
            ->buyCable(
                $data['data']['meta']['biller_name'],
                $data['data']['amount'],
                $data['data']['meta']['smartcard_number'],
                $data['data']['tx_ref'],
            );
        $transaction = Transaction::create([
            'user_id' => $data['data']['meta']['customer_id'],
            'reference' => $response['data']['reference'],
            'biller_type' => 'cable',
            'customer' => $response['data']['smartcard_number'],
            'biller_name' => $response['data']['biller_name'],
            'amount' => $response['data']['amount'],
            'time_of_purchase' => now(),
            'status' => $response['status']
        ]);
        event(new BillingPurchased($transaction));
        $user = User::find($data['data']['meta']['customer_id']);
        if (isset($data['data']['meta']['merchantLink']) && ($user->link !== $data['data']['meta']['merchantLink'])) {
            //do the merchant commission thing
            $amount = $response['data']['amount'];
            $merchantLink = $data['data']['meta']['merchantLink'];
            $merchantRole = Role::whereName('merchant')->first();
            $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

            if ($merchant) {
                $earning = Earning::firstOrCreate([
                    'user_id' => $merchant->id,
                ]);
                $earning->total_sales += $amount;
                $setting = Setting::find(1);
                $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                $earning->save();
                event(new MerchantLinkUsed($merchant));
            }
        }
        $path = config('app.frontend_url') . '/my-purchases?status=success';

        return $path;
    }

    public function buyCable(
        $type,
        $amount,
        $smartcard_number,
        $reference,
        $country = 'NG'
    ) {
        try {
            $response = $this
                ->flutterWave
                ->createBillPayment(
                    $country,
                    $smartcard_number,
                    $amount,
                    $type,
                    $reference
                );
            return $response;
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function buyWaec(
        $pinValue,
        $numberOfPins,
        $reference
    ) {
        try {
            $response = $this->baxipay->purchasePin(
                'waec',
                $pinValue,
                $numberOfPins,
                $reference
            );
            return $response;
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function buyPower(
        $type,
        $amount,
        $meter_number,
        $reference,
        $country = 'NG'
    ) {
        try {
            $response = $this
                ->flutterWave
                ->createBillPayment(
                    $country,
                    $meter_number,
                    $amount,
                    $type,
                    $reference
                );
            return $response;
        } catch (RequestException $e) {

            throw $e;
        }
    }

    public function verifyPaymentAndBuyData($transaction_id)
    {
        $data = $this->flutterWave->verifyTransaction($transaction_id);

        $response =  $this
            ->buyData(
                $data['data']['meta']['biller_name'],
                $data['data']['amount'],
                $data['data']['meta']['phone_number'],
                $data['data']['tx_ref'],
            );

        $transaction = Transaction::create([
            'user_id' => $data['data']['meta']['customer_id'],
            'reference' => $response['data']['reference'],
            'biller_type' => 'data',
            'network' => $response['data']['network'],
            'customer' => $response['data']['phone_number'],
            'biller_name' => $response['data']['network'],
            'amount' => $response['data']['amount'],
            'time_of_purchase' => now(),
            'status' => $response['status']
        ]);
        event(new BillingPurchased($transaction));
        $user = User::find($data['data']['meta']['customer_id']);
        if (isset($data['data']['meta']['merchantLink']) && ($user->link !== $data['data']['meta']['merchantLink'])) {
            //do the merchant commission thing
            $amount = $response['data']['amount'];
            $merchantLink = $data['data']['meta']['merchantLink'];
            $merchantRole = Role::whereName('merchant')->first();
            $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

            if ($merchant) {
                $earning = Earning::firstOrCreate([
                    'user_id' => $merchant->id,
                ]);
                $earning->total_sales += $amount;
                $setting = Setting::find(1);
                $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                $earning->save();
                event(new MerchantLinkUsed($merchant));
            }
        }
        $path = config('app.frontend_url') . '/my-purchases?status=success';
        return $path;
    }

    public function verifyPaymentAndBuyPower($transaction_id)
    {
        $data = $this->flutterWave->verifyTransaction($transaction_id);

        $response =  $this
            ->buyPower(
                $data['data']['meta']['biller_name'],
                $data['data']['amount'],
                $data['data']['meta']['meter_number'],
                $data['data']['tx_ref'],
            );


        $transaction = Transaction::create([
            'user_id' => $data['data']['meta']['customer_id'],
            'reference' => $response['data']['reference'],
            'biller_type' => 'power',
            'customer' => $response['data']['meter_number'],
            'biller_name' => $data['data']['biller_name'],
            'amount' => $response['data']['amount'],
            'time_of_purchase' => now(),
            'status' => $response['status']
        ]);
        event(new BillingPurchased($transaction));

        $user = User::find($data['data']['meta']['customer_id']);
        if (isset($data['data']['meta']['merchantLink']) && ($user->link !== $data['data']['meta']['merchantLink'])) {
            //do the merchant commission thing
            $amount = $response['data']['amount'];
            $merchantLink = $data['data']['meta']['merchantLink'];
            $merchantRole = Role::whereName('merchant')->first();
            $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

            if ($merchant) {
                $earning = Earning::firstOrCreate([
                    'user_id' => $merchant->id,
                ]);
                $earning->total_sales += $amount;
                $setting = Setting::find(1);
                $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                $earning->save();
                event(new MerchantLinkUsed($merchant));
            }
        }
        $path = config('app.frontend_url') . '/my-purchases?status=success';
        return $path;
    }

    public function verifyPaymentAndUpgradeUser($transaction_id)
    {

        $data = $this->flutterWave->verifyTransaction($transaction_id);

        Transaction::create([
            'user_id' => $data['data']['meta']['user_id'],
            'biller_name' => 'Upgrade',
            'reference' => $data['data']['tx_ref'],
            'amount' => $data['data']['amount'],
            'time_of_purchase' => now(),
            'status' => $data['status']
        ]);
        $merchantRole = Role::whereName('merchant')->first();
        $user = User::find($data['data']['meta']['user_id']);
        $user->role_id = $merchantRole->id;
        $user->save();
        Earning::create(['user_id' => $user->id]);
        $path = config('app.frontend_url') . '/my-purchases?upgrade=true';
        return $path;
    }


    public function verifyPaymentAndBuyWaec($transaction_id)
    {
        try {
            $data = $this->flutterWave->verifyTransaction($transaction_id);
            $saveCard = isset($data['data']['meta']['saveCard']) && (bool)$data['data']['meta']['saveCard'];
            $card = $data['data']['card'];
            $meta = $data['data']['meta'];
            $transaction = new Transaction;
            $transaction->user_id = $meta['customer_id'];
            $transaction->amount = $meta['amount'];
            $transaction->reference = $data['data']['tx_ref'];
            $transaction->biller_type = 'waec';
            $transaction->biller_name = $meta['biller_name'];

            try {
                $response = $this->buyWaec($meta['pinValue'], $meta['numberOfPins'], $data['data']['tx_ref']);
                $transaction->pin = json_encode($response['data']['pins']);
                $transaction->status = 'success';
                $transaction->save();
                event(new BillingPurchased($transaction));
                if ($saveCard) {
                    CardToken::create([
                        'first_6digits' => $card['first_6digits'],
                        'last_4digits' => $card['last_4digits'],
                        'token' => $card['token'],
                        'type' => $card['type'],
                        'user_id' => $data['data']['meta']['customer_id']
                    ]);
                }
                $path = config('app.frontend_url') . '/my-purchases?status=success';
                event(new BillingPurchased($transaction));
                $user = User::find($meta['customer_id']);
                if (isset($meta['merchantLink']) && ($user->link !== $data['data']['meta']['merchantLink'])) {
                    //do the merchant commission thing
                    $amount = $response['data']['amount'];
                    $merchantLink = $meta['merchantLink'];
                    $merchantRole = Role::whereName('merchant')->first();
                    $merchant = $merchantRole->users()->whereLink($merchantLink)->first();
                    if ($merchant) {

                        $earning = Earning::firstOrCreate([
                            'user_id' => $merchant->id,
                        ]);
                        $earning->total_sales += $amount;
                        $setting = Setting::find(1);
                        $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                        $earning->save();
                        event(new MerchantLinkUsed($merchant));
                    }
                }
                return $path;
            } catch (RequestException $e) {
                throw $e;
            }
            // if($data['status']==='successful')
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function verifyPaymentAndBuyAirtime($transaction_id)
    {
        try {
            $data = $this->flutterWave->verifyTransaction($transaction_id);
            $saveCard = isset($data['data']['meta']['saveCard']) && (bool)$data['data']['meta']['saveCard'];
            $card = $data['data']['card'];
            try {
                $response = $this->buyAirtime(
                    $data['data']['amount'],
                    $data['data']['meta']['phone_number'],
                    $data['data']['tx_ref'],
                );
                $transaction = Transaction::create([
                    'user_id' => $data['data']['meta']['customer_id'],

                    'reference' => $response['data']['reference'],
                    'biller_type' => 'airtime',
                    'network' => $response['data']['network'],
                    'customer' => $response['data']['phone_number'],
                    'biller_name' => 'AIRTIME',
                    'amount' => $response['data']['amount'],
                    'time_of_purchase' => now(),
                    'status' => $response['status']
                ]);

                if ($saveCard) {
                    CardToken::create([
                        'first_6digits' => $card['first_6digits'],
                        'last_4digits' => $card['last_4digits'],
                        'token' => $card['token'],
                        'type' => $card['type'],
                        'user_id' => $data['data']['meta']['customer_id']
                    ]);
                }

                $path = config('app.frontend_url') . '/my-purchases?status=success';
                event(new BillingPurchased($transaction));
                $user = User::find($data['data']['meta']['customer_id']);
                if (isset($data['data']['meta']['merchantLink']) && ($user->link !== $data['data']['meta']['merchantLink'])) {
                    //do the merchant commission thing
                    $amount = $response['data']['amount'];
                    $merchantLink = $data['data']['meta']['merchantLink'];
                    $merchantRole = Role::whereName('merchant')->first();
                    $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

                    if ($merchant) {

                        $earning = Earning::firstOrCreate([
                            'user_id' => $merchant->id,
                        ]);
                        $earning->total_sales += $amount;
                        $setting = Setting::find(1);
                        $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                        $earning->save();
                        event(new MerchantLinkUsed($merchant));
                    }
                }
                return $path;
            } catch (RequestException $e) {
                throw $e;
            }
        } catch (RequestException $e) {
            throw $e;
        }
    }

    public function initializeAirtimePayment(
        $amount,
        $phoneNumber,
        $useCard,
        $saveCard,
        $merchantLink = null
    ) {
        //Check if use card was selected && user has saved card just charge the person straight
        $user = User::find(auth()->id());
        $reference = 'Supra-' . Str::uuid()->toString();
        $transaction = new Transaction;
        $transaction->reference = $reference;
        $transaction->customer = $phoneNumber;
        $transaction->amount = $amount;
        $transaction->user_id = $user->id;
        $transaction->biller_type = 'Airtime';
        $transaction->biller_name = 'AIRTIME';


        if ($useCard && isset($user->savedCard)) {
            //charge with tokenized charge
            try {
                $tokenResponse = $this->flutterWave->tokenizedCharge($user->savedCard->token, $amount, $reference, 'NGN', $user->email);
                try {
                    //tokenise charge was a success so we can now buy the airtime
                    $response = $this->buyAirtime($amount, $phoneNumber, $reference);

                    if ($response) {
                        //Save transaction
                        $transaction->network = $response['data']['network'];
                        $transaction->time_of_purchase = now();
                        $transaction->status = 'success';
                        $transaction->save();
                        event(new BillingPurchased($transaction));
                    }


                    //pay merchant if link was used

                    if ($merchantLink && ($user->link !== $merchantLink)) {
                        //do the merchant commission thing

                        $merchantRole = Role::whereName('merchant')->first();
                        $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

                        if ($merchant) {

                            $earning = Earning::firstOrCreate([
                                'user_id' => $merchant->id,
                            ]);
                            $earning->total_sales += $amount;
                            $setting = Setting::find(1);
                            $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                            $earning->save();
                            event(new MerchantLinkUsed($merchant));
                        }
                    }
                    return 'Ok';
                } catch (RequestException $e) {
                    throw $e;
                }
            } catch (RequestException $e) {
                throw $e;
            }
        } else {
            //if the user didn't select use card, check if save card was selected

            $response = $this->flutterWave->chargeCard(
                $amount,
                'card',
                url('/payment/airtime-callback'),
                [
                    'email' => auth()->user()->email,
                    'name' => auth()->user()->name,
                    'phone' => auth()->user()->phone
                ],
                [
                    'customer_id' => auth()->id(),
                    'phone_number' => $phoneNumber,
                    'merchantLink' => $merchantLink,
                    'saveCard' => $saveCard,
                ]
            );
            return $response;
        }
    }

    public function initializeDataPayment(
        $amount,
        $billerName,
        $phoneNumber,
        $useCard,
        $saveCard,
        $merchantLink = null
    ) {

        //Check if use card was selected && user has saved card just charge the person straight
        $user = User::find(auth()->id());
        $reference = 'Supra-' . Str::uuid()->toString();
        $transaction = new Transaction;
        $transaction->reference = $reference;
        $transaction->customer = $phoneNumber;
        $transaction->amount = $amount;
        $transaction->user_id = auth()->id();
        $transaction->biller_type = 'Data Plan';
        $transaction->biller_name = $billerName;

        if ($useCard && isset($user->savedCard)) {
            try {
                $tokenResponse = $this->flutterWave->tokenizedCharge($user->savedCard->token, $amount, $reference, 'NGN', $user->email);
                try {
                    $response = $this->buyData($billerName, $amount, $phoneNumber, $reference);
                    $transaction->time_of_purchase = now();
                    $transaction->save();
                    event(new BillingPurchased($transaction));

                    //pay merchant if link was used

                    if ($merchantLink && ($user->link !== $merchantLink)) {
                        //do the merchant commission thing

                        $merchantRole = Role::whereName('merchant')->first();
                        $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

                        if ($merchant) {

                            $earning = Earning::firstOrCreate([
                                'user_id' => $merchant->id,
                            ]);
                            $earning->total_sales += $amount;
                            $setting = Setting::find(1);
                            $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                            $earning->save();
                            event(new MerchantLinkUsed($merchant));
                        }
                    }
                    return 'Ok';
                } catch (RequestException $e) {
                    throw $e;
                }
            } catch (RequestException $e) {
                throw $e;
            }
        } else {

            $response = $this->flutterWave->chargeCard(
                $amount,
                'card',
                url('/payment/data-callback'),
                [
                    'email' => auth()->user()->email,
                    'name' => auth()->user()->name,
                    'phone' => auth()->user()->phone
                ],
                [
                    'customer_id' => auth()->id(),
                    'biller_name' => $billerName,
                    'phone_number' => $phoneNumber,
                    'billingType' => 'data',
                    'saveCard' => $saveCard,
                    'merchantLink' => $merchantLink,

                ]
            );
            return $response;
        }
    }

    public function initializeCablePayment(
        $amount,
        $billerName,
        $smartcard_number,
        $useCard,
        $saveCard,
        $merchantLink = null
    ) {
        //Check if use card was selected && user has saved card just charge the person straight
        $user = User::find(auth()->id());
        $reference = 'Supra-' . Str::uuid()->toString();
        $transaction = new Transaction;
        $transaction->reference = $reference;
        $transaction->amount = $amount;
        $transaction->customer = $smartcard_number;
        $transaction->user_id = auth()->id();
        $transaction->biller_type = 'Cable Plan';
        $transaction->biller_name = $billerName;


        if ($useCard && isset($user->savedCard)) {
            try {
                $tokenResponse = $this->flutterWave->tokenizedCharge($user->savedCard->token, $amount, $reference, 'NGN', $user->email);
                Log::alert('Tokenized charge done');
                try {
                    $response = $this->buyCable($billerName, $amount, $smartcard_number, $reference);
                    $transaction->time_of_purchase = now();
                    $transaction->save();
                    event(new BillingPurchased($transaction));
                    //pay merchant if link was used
                    if ($merchantLink && ($user->link !== $merchantLink)) {
                        //do the merchant commission thing

                        $merchantRole = Role::whereName('merchant')->first();
                        $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

                        if ($merchant) {

                            $earning = Earning::firstOrCreate([
                                'user_id' => $merchant->id,
                            ]);
                            $earning->total_sales += $amount;
                            $setting = Setting::find(1);
                            $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                            $earning->save();
                            event(new MerchantLinkUsed($merchant));
                        }
                    }
                    return 'Ok';
                } catch (RequestException $e) {
                    throw $e;
                }
            } catch (RequestException $e) {
                throw $e;
            }
        } else {

            $response = $this->flutterWave->chargeCard(
                $amount,
                'card',
                url('/payment/cable-callback'),
                [
                    'email' => auth()->user()->email,
                    'name' => auth()->user()->name,
                    'phone' => auth()->user()->phone
                ],
                [
                    'customer_id' => auth()->id(),
                    'biller_name' => $billerName,
                    'smartcard_number' => $smartcard_number,
                    'merchantLink' => $merchantLink,
                    'saveCard' => $saveCard

                ],
                [
                    'title' => 'Supra Cable Payment',
                    'description' => 'Pay your bill for ' . $smartcard_number
                ]
            );
            return $response;
        }
    }

    public function initializeWaecPayment(
        $pinValue,
        $numberOfPins,
        $pinDescription,
        $useCard,
        $saveCard,
        $merchantLink = null
    ) {
        $user = User::find(auth()->id());
        $amount = (int)$pinValue * (int)$numberOfPins;
        $reference = 'Supra-' . Str::uuid()->toString();
        $transaction = new Transaction;
        $transaction->reference = $reference;
        $transaction->amount = $amount;
        $transaction->biller_type = 'waec';
        $transaction->user_id = $user->id;
        $transaction->biller_name = $pinDescription;



        if ($useCard && isset($user->savedCard)) {
            try {
                $tokenResponse = $this->flutterWave->tokenizedCharge($user->savedCard->token, $amount, $reference, 'NGN', $user->email);

                try {
                    $response =  $this->buyWaec(
                        $pinValue,
                        $numberOfPins,
                        $reference
                    );

                    $transaction->time_of_purchase = now();
                    $transaction->pin = json_encode($response['data']['pins']);;
                    $transaction->save();
                    event(new BillingPurchased($transaction));

                    //pay merchant if link was used

                    if ($merchantLink && ($user->link !== $merchantLink)) {
                        //do the merchant commission thing

                        $merchantRole = Role::whereName('merchant')->first();
                        $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

                        if ($merchant) {

                            $earning = Earning::firstOrCreate([
                                'user_id' => $merchant->id,
                            ]);
                            $earning->total_sales += $amount;
                            $setting = Setting::find(1);
                            $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                            $earning->save();
                            event(new MerchantLinkUsed($merchant));
                        }
                        return 'Ok';
                    }
                } catch (RequestException $e) {
                    throw $e;
                }
            } catch (RequestException $e) {
                throw $e;
            }
        } else {
            //not use card
            $response = $this->flutterWave->chargeCard(
                $amount,
                'card',
                url('/payment/waec-callback'),
                [
                    'email' => $user->email,
                    'phone' => $user->phone,
                    'name' => $user->name,
                ],
                [
                    'customer_id' => $user->id,
                    'amount' => $amount,
                    'pinValue' => $pinValue,
                    'numberOfPins' => $numberOfPins,
                    'biller_name' => $pinDescription,
                    'saveCard' => $saveCard,
                    'merchantLink' => $merchantLink,
                ]
            );
            return $response;
        }
    }

    public function initializePowerPayment(
        $amount,
        $billerName,
        $meter_number,
        $useCard,
        $saveCard,
        $merchantLink = null
    ) {
        //Check if use card was selected && user has saved card just charge the person straight
        $user = User::find(auth()->id());
        $reference = 'Supra-' . Str::uuid()->toString();
        $transaction = new Transaction();
        $transaction->reference = $reference;
        $transaction->amount = $amount;
        $transaction->customer = $meter_number;
        $transaction->user_id = auth()->id();
        $transaction->biller_type = 'Power Payment';
        $transaction->biller_name = $billerName;


        if ($useCard && isset($user->savedCard)) {
            try {
                $tokenResponse = $this->flutterWave->tokenizedCharge($user->savedCard->token, $amount, $reference, 'NGN', $user->email);
                try {
                    $response = $this->buyPower($billerName, $amount, $meter_number, $reference);
                    $transaction->time_of_purchase = now();
                    $transaction->save();
                    event(new BillingPurchased($transaction));

                    //pay merchant if link was used

                    if ($merchantLink && ($user->link !== $merchantLink)) {
                        //do the merchant commission thing

                        $merchantRole = Role::whereName('merchant')->first();
                        $merchant = $merchantRole->users()->whereLink($merchantLink)->first();

                        if ($merchant) {

                            $earning = Earning::firstOrCreate([
                                'user_id' => $merchant->id,
                            ]);
                            $earning->total_sales += $amount;
                            $setting = Setting::find(1);
                            $earning->my_earnings += ceil(($setting->commissionPercentage / 100) * $amount);
                            $earning->save();
                            event(new MerchantLinkUsed($merchant));
                        }
                    }
                    return 'Ok';
                } catch (RequestException $e) {
                    throw $e;
                }
            } catch (RequestException $e) {
                throw $e;
            }
        } else {

            $response = $this->flutterWave->chargeCard(
                $amount,
                'card',
                url('payment/power-callback'),
                [
                    'name' => auth()->user()->name,
                    'email' => auth()->user()->email,
                    'phone' => auth()->user()->phone,
                ],
                [
                    'customer_id' => auth()->id(),
                    'biller_name' => $billerName,
                    'meter_number' => $meter_number,
                    'merchantLink' => $merchantLink,
                    'saveCard' => $saveCard
                ],
                [
                    'title' => 'Supra Electricty Payment',
                    'description' => 'Pay your electric bill for' . $meter_number
                ]
            );
            return $response;
        }
    }

    public function initializeUpgradePayment()
    {
        $setting = Setting::find(1);
        $response = $this->flutterWave->chargeCard($setting->upgradeSum, 'card', url('payment/upgrade-callback'), [
            'name' => auth()->user()->name,
            'email' => auth()->user()->email,
            'phone' => auth()->user()->phone
        ], ['user_id' => auth()->id()], ['title' => 'Upgrade to merchant']);
        return $response;
    }
}
