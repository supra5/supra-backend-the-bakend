<?php

namespace Supra\Services\Payment;

interface BaxipayInterface
{

    public function fetchPinBundles($serviceType);

    public function purchasePin($serviceType, $pinValue, $numberOfPins, $agentReference);
}
