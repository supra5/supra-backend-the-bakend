<?php

namespace Supra\Services\Payment;

use Exception;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class Flutterwave implements FlutterwaveInterface
{
    private $apiUrl;
    private $publicKey;
    private $secretKey;


    public function __construct($apiUrl, $publicKey, $secretKey)
    {
        $this->apiUrl = $apiUrl;
        $this->publicKey = $publicKey;
        $this->secretKey = $secretKey;
    }

    private function makeRequest()
    {
        return Http::withToken($this->secretKey)->acceptJson()->baseUrl($this->apiUrl);
    }

    public function chargeCard(
        $amount,
        $paymentOption,
        $redirect_url,
        $customer,
        $meta,
        $customizations = []
    ) {
        $response = $this->makeRequest()->post('/payments', [
            'amount' => $amount,
            'currency' => 'NGN',
            'tx_ref' => 'supra-' . Str::uuid(),
            'redirect_url' => $redirect_url,
            'customer' => $customer,
            'meta' => $meta,
            'customizations' => $customizations
        ]);
        if ($response->successful()) {
            return $response->json();
        } else {
            $response->throw()->json('message');
        }
    }

    public function verifyTransaction($transaction_id)
    {
        return $this
            ->makeRequest()
            ->get('/transactions/' . $transaction_id . '/verify')
            ->throw()
            ->json();
    }

    public function createBillPayment($country, $customer, $amount, $type, $reference)
    {
        $response = $this->makeRequest()->post('bills', [
            'country' => $country,
            'customer' => $customer,
            'amount' => $amount,
            'type' => $type,
            'reference' => $reference,
            'recurrence' => 'ONCE',
        ])->throw()->json();

        return $response;
    }

    public function validateBillService($item_code, $biller_code, $customer)
    {
        return  $this->makeRequest()->get('bill-items/' . $item_code . '/validate', [
            'code' => $biller_code,
            'customer' => $customer
        ])
            ->throw()->json('status');
    }

    public function verifyAccountNumber($accountNumber, $accountBank)
    {
        // TODO implement account number verification
        // $response = $this->makeRequest()->get('/accounts/resolve');
    }

    public function transfer($accountNumber, $accountBank, $amount, $reference, $narration, $currency = "NGN")
    {
        return  $this->makeRequest()->post('/transfers', [
            'account_bank' => $accountBank,
            'account_number' => $accountNumber,
            'amount' => $amount,
            'narration' => $narration,
            'currency' => $currency,
            'reference' => $reference,
        ])->throw()->json();
    }

    public function tokenizedCharge(
        $cardToken,
        $amount,
        $tx_ref,
        $currency,
        $email
    ) {
        $response = $this->makeRequest()->post('/tokenized-charges', [
            "amount" => $amount,
            "tx_ref" => $tx_ref,
            "email" => $email,
            "currency" => $currency,
            "token" => $cardToken,

        ])->throw()->json();
        return $response;
    }
}
