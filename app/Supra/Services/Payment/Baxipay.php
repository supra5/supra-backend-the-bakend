<?php

namespace Supra\Services\Payment;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class Baxipay implements BaxipayInterface
{
    private $apiUrl;
    private $apiKey;
    private $userSecret;
    private $username;


    public function __construct($apiUrl, $apiKey, $userSecret, $username)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        $this->userSecret = $userSecret;
        $this->username = $username;
    }

    private function makeRequest()
    {
        return Http::withHeaders(['x-api-key' => $this->apiKey, 'Baxi-date' => Carbon::now()->toRfc822String()])->acceptJson()->baseUrl($this->apiUrl);
    }

    public function fetchPinBundles($serviceType)
    {
        return  $this->makeRequest()->post('/services/epin/bundles', [
            'service_type' => $serviceType
        ])->throw()->json();
    }

    public function purchasePin($serviceType, $pinValue, $numberOfPins, $agentReference)
    {
        return $this->makeRequest()->post('/services/epin/request', [
            'service_type' => $serviceType,
            'pinValue' => $pinValue,
            'numberOfPins' => $numberOfPins,
            'amount' => (int)$numberOfPins * (int)$pinValue,
            'agentReference' => $agentReference,
            'agentId' => $this->username
        ])->throw()->json();
    }
}
