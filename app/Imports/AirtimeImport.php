<?php

namespace App\Imports;

use App\Models\BulkAirtime;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class AirtimeImport implements ToModel, WithHeadingRow, WithChunkReading
{
    public function model(array $row)
    {

        return new BulkAirtime([
            'customer' => $row['CUSTOMER'],
            'amount' => $row['AMOUNT'],
            'user_id' => auth()->id(),
        ]);
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
