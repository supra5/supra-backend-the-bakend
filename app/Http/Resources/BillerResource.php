<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BillerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'biller_code' => $this->biller_code,
            'item_code' => $this->item_code,
            'image' => asset($this->image),
            'short_name' => $this->short_name,
            'biller_name' => $this->biller_name,
            'amount' => $this->amount
        ];
    }
}
