<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'reference' => $this->reference,
            'biller_type' => $this->biller_type,
            'biller_name' => $this->biller_name,
            'amount' => $this->amount,
            'time_of_purchase' => $this->time_of_purchase,
            'pin' => $this->pin ? json_decode($this->pin) : null,
            'customer' => $this->customer,
            'status' => $this->status,
            'network' => $this->network ?? null,
        ];
    }
}
