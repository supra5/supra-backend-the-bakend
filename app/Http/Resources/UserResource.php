<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->email,
            'bank_id' => $this->bank_id,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'isVerified' => $this->email_verified_at ? true : false,
            'role' => $this->role,
        ];
        $data['hasCard'] = $this->savedCard ? true : false;
        if ($this->role->name === 'merchant') {
            $data['link'] = $this->link;
            $data['earning'] = $this->earning;
            $data['bank'] = $this->bank;
            $data['account_number'] = $this->account_number;
            if ($this->savedCard) {
                $data['card_last_digits'] = $this->savedCard->last_4digits;
            }
        }
        return $data;
    }
}
