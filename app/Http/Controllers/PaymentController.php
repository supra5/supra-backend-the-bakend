<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Supra\Services\Payment\BillingServiceInterface;



//TODO workout the transaction ID stuff of flutterwave
class PaymentController extends Controller
{
    //
    private $billingService;

    public function __construct(BillingServiceInterface $billingService)
    {
        $this->billingService = $billingService;
    }


    public function handleAirtimeCallback(Request $request)
    {
        if ($request->get('status') === 'cancelled') {
            $path = config('app.frontend_url') . '/my-purchases?status=cancelled';
            return redirect($path);
        }
        try {
            $path = $this->billingService->verifyPaymentAndBuyAirtime($request->get('transaction_id'));
            return redirect($path);
        } catch (RequestException $e) {
            $path = config('app.frontend_url') . '/my-purchases?status=error&message=' . urlencode($e->response->json('message'));
            return redirect($path);
        }
    }

    public function handleDataCallback(Request $request)
    {
        if ($request->get('status') === 'cancelled') {
            $path = config('app.frontend_url') . '/my-purchases?status=cancelled';
            return redirect($path);
        }
        try {
            $path = $this->billingService->verifyPaymentAndBuyData($request->get('transaction_id'));
            return redirect($path);
        } catch (RequestException $e) {
            $path = config('app.frontend_url') . '/my-purchases?status=error&message=' . urlencode($e->response->json('message'));
            return redirect($path);
        }
    }

    public function handleCableCallback(Request $request)
    {
        if ($request->get('status') === 'cancelled') {
            $path = config('app.frontend_url') . '/my-purchases?status=cancelled';
            return redirect($path);
        }
        try {
            $path = $this->billingService->verifyPaymentAndBuyCable($request->get('transaction_id'));
            return redirect($path);
        } catch (RequestException $e) {
            $path = config('app.frontend_url') . '/my-purchases?status=error&message=' . urlencode($e->response->json('message'));
            return redirect($path);
        }
    }

    public function handlePowerCallback(Request $request)
    {
        if ($request->get('status') === 'cancelled') {
            $path = config('app.frontend_url') . '/my-purchases?status=cancelled';
            return redirect($path);
        }
        try {
            $path = $this->billingService->verifyPaymentAndBuyPower($request->get('transaction_id'));
            return redirect($path);
        } catch (RequestException $e) {
            $path = config('app.frontend_url') . '/my-purchases?status=error&message=' . urlencode($e->response->json('message'));
            return redirect($path);
        }
    }

    public function handleUpgradeCallback(Request $request)
    {
        if ($request->get('status') === 'cancelled') {
            $path = config('app.frontend_url') . '/my-purchases?status=cancelled';
            return redirect($path);
        }
        try {
            $path = $this->billingService->verifyPaymentAndUpgradeUser($request->get('transaction_id'));
            return redirect($path);
        } catch (RequestException $e) {
            $path = config('app.frontend_url') . '/my-purchases?status=error&message=' . urlencode($e->response->json('message'));
            return redirect($path);
        }
    }

    public function handleWaecCallback(Request $request)
    {
        if ($request->get('status') === 'cancelled') {
            $path = config('app.frontend_url') . '/my-purchases?status=cancelled';
            return redirect($path);
        }
        try {
            $path = $this->billingService->verifyPaymentAndBuyWaec($request->get('transaction_id'));
            return redirect($path);
        } catch (RequestException $e) {
            $path = config('app.frontend_url') . '/my-purchases?status=error&message=' . urlencode($e->response->json('message'));
            return redirect($path);
        }
    }
}
