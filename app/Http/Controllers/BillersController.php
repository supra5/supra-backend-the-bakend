<?php

namespace App\Http\Controllers;

use App\Http\Resources\BillerResource;
use Illuminate\Http\Request;
use App\Models\Biller;
use Illuminate\Http\Client\RequestException;
use Supra\Services\Payment\BaxipayInterface;

class BillersController extends Controller
{
    //
    private $baxipay;
    public function __construct(BaxipayInterface $baxipay)
    {
        $this->baxipay = $baxipay;
    }


    public function getPowerBillers()
    {
        $billers = Biller::where(['country' => "NG", 'is_airtime' => false])
            ->where(function ($query) {
                $query->orWhere('name', 'like', '%electricity%')
                    ->orWhere('name', 'like', '%electric%')
                    ->orWhere('name', 'like', '%disco%')
                    ->orWhere('name', 'like', '%edc%');
            })
            ->orderBy('name')->get();
        return response()->json(['billers' => collect(BillerResource::collection($billers))->unique('biller_name')->values()]);
    }
    public function getAirtimeBillers()
    {
        $billers = Biller::where('is_airtime', true)->get();
        return response()->json(['billers' => BillerResource::collection($billers)]);
    }

    public function getDataBundleBillers(Request $request)
    {
        $billers = Biller::where(['country' => "NG", 'is_airtime' => false])
            ->where(function ($query) {
                $query->where('name', 'like', '%mtn%')
                    ->orWhere('name', 'like', '%airtel%')
                    ->orWhere('name', 'like', '%glo%')
                    ->orWhere('name', 'like', '%9mobile%');
            })
            ->get();
        return response()->json(['billers' => collect(BillerResource::collection($billers)->toArray($request))->unique('biller_name')->all()]);
    }

    public function getCableBillers(Request $request)
    {
        $billers = Biller::where(['country' => "NG", 'is_airtime' => false])
            ->where(function ($query) {
                $query->orWhere('name', 'like', '%dstv%')
                    ->orWhere('name', 'like', '%startimes%')
                    ->orWhere('name', 'like', '%gotv%');
            })
            ->get();
        return response()->json(['billers' => collect(BillerResource::collection($billers))->unique('biller_name')->values()]);
    }

    public function getWaecBillers(Request $request)
    {
        try {
            $response = $this->baxipay->fetchPinBundles('waec');
            return response()->json(['billers' => $response['data']]);
        } catch (RequestException $e) {
            return response()->json(['message' => $e->response->message]);
        }
    }
}
