<?php

namespace App\Http\Controllers;

use App\Http\Requests\AirtimeRequest;
use App\Http\Requests\CableRequest;
use App\Http\Requests\DataRequest;
use App\Http\Requests\PowerRequest;
use App\Http\Requests\WaecRequest;
use App\Imports\AirtimeImport;
use App\Models\User;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use League\CommonMark\Extension\SmartPunct\EllipsesParser;
use Supra\Services\Payment\BillingServiceInterface;
use Maatwebsite\Excel\Importer;



class BillingController extends Controller
{
    //
    private $billingService;
    private $importer;
    public function __construct(BillingServiceInterface $billingService, Importer $importer)
    {
        $this->billingService = $billingService;
        $this->importer = $importer;
    }

    public function buyData(DataRequest $request)
    {
        Log::alert('Started buy airtime');
        $merchantLink = $request->get('link') ?: null;
        $saveCard = (bool)$request->post('saveCard');
        $useCard = (bool)$request->post('useCard');
        $user = User::find(auth()->id());
        try {
            $res = $this
                ->billingService
                ->initializeDataPayment(
                    $request
                        ->post('amount'),
                    $request->post('biller_name'),
                    $request->post('phone_number'),
                    $useCard,
                    $saveCard,
                    $merchantLink
                );
            if ($useCard && isset($user->savedCard)) {
                return response()->json(['message' => $res], 200);
            } else {
                return response()->json($res, 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }

    public function buyAirtime(AirtimeRequest $request)
    {
        $merchantLink = $request->get('link') ?: null;
        $saveCard = (bool)$request->post('saveCard');
        $useCard = (bool)$request->post('useCard');
        $user = User::find(auth()->id());

        try {
            $res = $this->billingService
                ->initializeAirtimePayment(
                    $request->post('amount'),
                    $request->post('phone_number'),
                    $useCard,
                    $saveCard,
                    $merchantLink,
                );
            if ($useCard && isset($user->savedCard)) {
                return response()->json(['message' => $res], 200);
            } else {
                return response()->json($res, 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }



    public function payCable(CableRequest $request)
    {

        $merchantLink = $request->get('link') ?: null;
        $saveCard = (bool)$request->post('saveCard');
        $useCard = (bool)$request->post('useCard');
        $user = User::find(auth()->id());
        try {
            $res = $this->billingService->initializeCablePayment(
                $request->post('amount'),
                $request->post('biller_name'),
                $request->post('smartcard_number'),
                $useCard,
                $saveCard,
                $merchantLink
            );
            if ($useCard && isset($user->saveCard)) {
                return response()->json(['message' => $res], 200);
            } else {
                return response()->json($res, 200);
            }
        } catch (RequestException $e) {
            return response()->json(['message' => $e->response->json('message')], 400);
        }
    }

    public function buyWaec(WaecRequest $request)
    {
        $merchantLink = $request->get('link') ?: null;
        $saveCard = $request->post('saveCard') ?: false;
        $useCard = $request->post('useCard') ?: false;
        $user = User::find(auth()->id());


        try {
            $res = $this->billingService->initializeWaecPayment(
                $request->post('pinValue'),
                $request->post('numberOfPins'),
                $request->post('pinDescription'),
                $useCard,
                $saveCard,
                $merchantLink
            );
            if ($useCard && isset($user->savedCard)) {
                return response()->json(['message' => $res], 200);
            } else {
                return response()->json($res, 200);
            }
        } catch (RequestException $e) {
            return response()->json(['message' => $e->response->json('message')], 400);
        }
    }

    public function buyPower(PowerRequest $request)
    {
        $merchantLink = $request->get('link') ?: null;
        $saveCard = $request->post('saveCard') ?: false;
        $useCard = $request->post('useCard') ?: false;
        $user = User::find(auth()->id());
        try {
            $res = $this->billingService->initializePowerPayment(
                $request->post('amount'),
                $request->post('biller_name'),
                $request->post('meter_number'),
                $useCard,
                $saveCard,
                $merchantLink
            );
            if ($useCard && isset($user->savedCard)) {
                return response()->json(['message' => $res], 200);
            } else {
                return response()->json($res, 200);
            }
        } catch (RequestException $e) {
            return response()->json(['message' => $e->response->json('message')], 400);
        }
    }

    public function initiateUpgrade(Request $request)
    {
        try {
            $res = $this->billingService->initializeUpgradePayment();
            return response()->json($res, 200);
        } catch (\Exception $e) {
            return response()->json(['message', $e->getMessage()], 400);
        }
    }

    public function buyBulkAirtime(Request $request)
    {
        $request->validate([
            'csvFile' => 'required|mimes:csv,txt,xlsx',
        ]);
        $path = $request->file('csvFile')->store('public/uploads');

        $this->importer->import(new AirtimeImport, $path);
        return response()->json('Ok');
    }
}
