<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Supra\Services\AuthServiceInterface;

class LoginController extends Controller
{
    //
    private $authService;
    public function __construct(AuthServiceInterface $authService)
    {
        $this->authService = $authService;
    }

    public function login(Request $request)
    {
        try {
            $res = $this->authService->login($request->post('email'), $request->post('password'));
            return response()->json(['token' => $res['token'], 'user' => new UserResource($res['user'])]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }
    }

    public function logout()
    {
    }
}
