<?php

namespace App\Http\Controllers\Auth;

use App\Events\MerchantRequestWithdrawal;
use App\Http\Controllers\Controller;
use App\Http\Requests\BankRequest;
use App\Http\Resources\UserResource;
use App\Models\CardToken;
use App\Models\User;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;
use Supra\Services\AuthServiceInterface;
use Illuminate\Support\Str;
use Supra\Services\Payment\FlutterwaveInterface;

class UserController extends Controller
{
    //
    private $authService;
    private $flutterwave;
    public function __construct(AuthServiceInterface $authService, FlutterwaveInterface $flutterwaveService)
    {
        $this->authService = $authService;
        $this->flutterwave = $flutterwaveService;
    }
    public function user(Request $request)
    {
        try {
            $user = $this->authService->user();
            return response()->json(['user' => new UserResource($user)]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 401);
        }
    }

    public function logout(Request $request)
    {
        try {
            $this->authService->logout($request);
            return response()->json(['status' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage], 400);
        }
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'oldPassword' => ['required', 'current_password:sanctum'],
            'newPassword' => ['required', Password::min(8)->numbers(), 'confirmed']
        ]);

        $user = User::find(auth()->id());
        $user->forceFill([
            'password' => bcrypt($request->post('newPassword'))
        ])->save();
        $request->user()->tokens()->delete();
        $token = $user->createToken('Login Token')->plainTextToken;
        return response()->json(['token' => $token]);
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users')->ignore(auth()->id())],
            'phone' => ['required', 'phone:NG',  Rule::unique('users')->ignore(auth()->id())]
        ]);
        $user = User::find(auth()->id());

        $user->fill([
            'name' => $request->post('name'),
            'email' => $request->post('email'),
            'phone' => $request->post('phone'),
        ]);
        $user->save();
        return response()->json(['user' => new UserResource($user)]);
    }

    public function generateLink(Request $request)
    {
        if ($request->user()->role->name === 'merchant') {
            $appUrl = config('app.frontend_url');
            $code = Str::random(7);
            $link = $appUrl . "?link={$code}";
            $user = $request->user();
            $user->link = $link;
            $user->save();
            return response()->json(['user' => new UserResource($user)], 200);
        } else {
            return response()->json(['message' => 'You are not a merchant'], 400);
        }
    }

    public function updateBankSetting(BankRequest $request)
    {
        $user = User::find(auth()->id());
        $user->account_number = $request->post('account_number');
        $user->bank_id = $request->post('bank');
        $user->save();
        return response()->json(['user' => new UserResource($user)]);
    }

    public function withdrawEarning(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric|min:3000'
        ]);
        $user = User::find(auth()->id());
        $amount = $request->post('amount');
        $earning = $user->earning;
        if ($amount > $earning->my_earnings) {
            return response()->json(['message' => 'Amount requested greater than withdrawable amount'], 400);
        }

        if ($amount < 3000) {
            return response()->json(['message' => 'Mininum withdrawable amount is 3000 naira.'], 400);
        }

        try {
            $response = $this->flutterwave->transfer(
                $user->account_number,
                $user->bank->code,
                $amount,
                'supra-' . Str::uuid()->toString(),
                'Supra Merchant Payout'
            );
            $earning->total_earnings += floatval($amount);
            $earning->my_earnings -= floatval($amount);
            $earning->save();
            event(new MerchantRequestWithdrawal($user, $amount));
            return response()->json(['user' => new UserResource($user)]);
        } catch (RequestException $e) {
            return response()->json($e->response->json(), 400);
        }
    }

    public function deleteSavedCard(Request $request)
    {
        $cardToken = CardToken::where('user_id', auth()->id())->first();
        if ($cardToken) {
            $cardToken->delete();
        }
        $user = User::find(auth()->id());
        return response()->json(['user' => new UserResource($user)]);
    }
}
