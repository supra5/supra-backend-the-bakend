<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;
use App\Models\User;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'unique:users'],
            'phone' => ['required', 'unique:users', 'phone:NG'],
            'name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'confirmed', Password::min(8)->numbers()],
        ]);
        $clientRole = Role::whereName('client')->first();
        $user = User::create([
            'email' => $request->post('email'),
            'role_id' => $clientRole->id,
            'phone' => $request->post('phone'),
            'name' => $request->post('name'),
            'password' => bcrypt($request->post('password'))
        ]);
        $token = $user->createToken('Login Token')->plainTextToken;
        return response()->json(['token' => $token, 'user' => new UserResource($user)]);
    }
}
