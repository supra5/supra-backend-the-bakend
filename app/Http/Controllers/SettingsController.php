<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    //

    public function getMerchantSettings()
    {
        $setting = Setting::find(1);
        return response()->json([
            'upgradeSum' => $setting->upgradeSum,
            'commissionPercentage' => $setting->commissionPercentage
        ]);
    }

    public function updateMerchantSettings(Request $request)
    {
        if (auth()->user()->role->name !== 'admin') {
            return response()->json(['message' => 'You are not authorized to perform action'], 400);
        }
        $request->validate([
            'upgradeSum' => 'required|numeric',
            'commissionPercentage' => 'required|numeric'
        ]);

        $setting = Setting::find(1);
        $setting->upgradeSum = $request->post('upgradeSum');
        $setting->commissionPercentage = $request->post('commissionPercentage');
        $setting->save();
        return response()->json([
            'upgradeSum' => $setting->upgradeSum,
            'commissionPercentage' => $setting->commissionPercentage
        ]);
    }
}
