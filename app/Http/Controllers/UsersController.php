<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UsersController extends Controller
{
    //

    public function getClients(Request $request)
    {
        $clientRole = Role::whereName('client')->first();
        $users = $clientRole->users()->orderBy('name')->paginate($request->perPage);
        return response()->json($users);
    }

    public function addClient(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => ['required', Password::min(8)->numbers()]
        ]);
        $clientRole = Role::whereName('client')->first();
        $client = $clientRole->users()->create([
            'email' => $request->post('email'),
            'name' => $request->post('name'),
            'password' => bcrypt($request->post('password')),
            'phone' => $request->post('phone'),
            'email_verified_at' => now()
        ]);
        return response()->json(['client' => new UserResource($client)]);
    }

    public function deleteClient(Request $request)
    {
        $clientRole = Role::whereName('client')->first();
        $client = $clientRole->users()->find($request->id);
        $client->delete();
        return response()->json('ok');
    }

    public function countClients()
    {
        $clientRole = Role::whereName('client')->first();
        $count = $clientRole->users()->count();
        return response()->json(['count' => $count]);
    }

    //Merchant

    public function getMerchants(Request $request)
    {
        $clientRole = Role::whereName('merchant')->first();
        $users = $clientRole->users()->orderBy('name')->paginate($request->perPage);
        return response()->json($users);
    }

    public function addMerchant(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => ['required', Password::min(8)->numbers()]
        ]);
        $clientRole = Role::whereName('merchant')->first();
        $client = $clientRole->users()->create([
            'email' => $request->post('email'),
            'name' => $request->post('name'),
            'password' => bcrypt($request->post('password')),
            'phone' => $request->post('phone'),
            'email_verified_at' => now()
        ]);
        return response()->json(['client' => new UserResource($client)]);
    }

    public function deleteMerchant(Request $request)
    {
        $clientRole = Role::whereName('merchant')->first();
        $client = $clientRole->users()->find($request->id);
        $client->delete();
        return response()->json('ok');
    }

    public function countMerchants()
    {
        $merchantRole = Role::whereName('merchant')->first();
        $count = $merchantRole->users()->count();
        return response()->json(['count' => $count]);
    }

    //Admin

    public function getAdmins(Request $request)
    {
        $clientRole = Role::whereName('admin')->first();
        $users = $clientRole->users()->where('id', '<>', auth()->id())->orderBy('name')->paginate($request->perPage);
        return response()->json($users);
    }

    public function addAdmin(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => ['required', 'email', Rule::unique('users')],
            'password' => ['required', Password::min(8)->numbers()]
        ]);
        $clientRole = Role::whereName('admin')->first();
        $client = $clientRole->users()->create([
            'email' => $request->post('email'),
            'name' => $request->post('name'),
            'password' => bcrypt($request->post('password')),
            'phone' => $request->post('phone'),
            'email_verified_at' => now()
        ]);
        return response()->json(['client' => new UserResource($client)]);
    }

    public function deleteAdmin(Request $request)
    {
        $clientRole = Role::whereName('admin')->first();
        $client = $clientRole->users()->find($request->id);
        $client->delete();
        return response()->json('ok');
    }
}
