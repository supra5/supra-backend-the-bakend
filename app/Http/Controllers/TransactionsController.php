<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionCollection;
use App\Http\Resources\TransactionResource;
use App\Models\Bank;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    //

    public function getAllTransactions(Request $request)
    {
        $transactions = Transaction::with('user')->latest()->paginate($request->perPage);
        return response()->json(new TransactionCollection($transactions));
    }

    public function getMyTransactions(Request $request)
    {
        $transactions = auth()->user()->transactions()->latest()->paginate($request->perPage);
        return response()->json(new TransactionCollection($transactions));
    }

    public function getMyLimitedTransactions(Request $request)
    {
        $transactions = auth()->user()->transactions()->latest()->limit(10)->get();
        return response()->json($transactions);
    }

    public function limitedTransactions()
    {
        $transactions = Transaction::with('user')->latest()->limit(15)->get();
        return response()->json(['transactions' => $transactions]);
    }

    public function getTotalTransactions()
    {
        $amount = Transaction::sum('amount');
        return response()->json(['amount' => $amount]);
    }

    public function getAllBanks()
    {
        $banks = Bank::orderBy('name')->get();
        return response()->json(['banks' => $banks]);
    }
}
