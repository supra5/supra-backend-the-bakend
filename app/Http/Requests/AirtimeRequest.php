<?php

namespace App\Http\Requests;

use App\Models\Biller;
use Illuminate\Foundation\Http\FormRequest;
use Supra\Services\Payment\FlutterwaveInterface;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Log;

class AirtimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(FlutterwaveInterface $flutterwaveService)
    {
        //Find the biller and use it to validate the phone number
        //TODO validate phone number


        $rules = [
            'amount' => 'required',
            'network' => 'required|exists:billers,biller_code',
            "phone_number" => ['required', 'phone:NG', function ($attribute, $value, $fail) use ($flutterwaveService) {
                $biller = Biller::where('biller_code', $this->post('network'))->first();
                try {
                    $status = $flutterwaveService->validateBillService($biller->item_code, $biller->biller_code, "{$value}");
                    if ($status !== 'success') {
                        $fail('The ' . $attribute . ' is invalid');
                    }
                } catch (RequestException $e) {
                    $fail("Invalid " . $attribute);
                }
            }],
        ];

        return $rules;
    }

    public function attributes()
    {
        return [
            'amount' => 'amount',
            'network' => 'network',
            'phoneNumber' => 'phone number'
        ];
    }

    public function messages()
    {
        return [
            'phoneNumber.phone' => 'Must be a valid phone number'
        ];
    }
}
