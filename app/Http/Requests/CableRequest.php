<?php

namespace App\Http\Requests;

use App\Models\Biller;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Log;
use Supra\Services\Payment\FlutterwaveInterface;

class CableRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(FlutterwaveInterface $flutterwave)
    {
        return [
            'biller_name' => ['required', 'exists:billers,biller_name'],
            'smartcard_number' => ['required', function ($attribute, $value, $fail) use ($flutterwave) {
                $biller = Biller::where('biller_name', $this->input('biller_name'))->first();

                if (!$biller) {
                    $fail('Invalid smartcard number');
                }
                if (!config('app.debug')) {
                    try {
                        $flutterwave->validateBillService($biller->item_code, $biller->biller_code, $value);
                    } catch (\Illuminate\Http\Client\RequestException $e) {
                        Log::error($e->response->json());
                        $fail($e->response->json('message'));
                    }
                }
            }],
            'amount' => ['required', 'numeric'],

        ];
    }
}
