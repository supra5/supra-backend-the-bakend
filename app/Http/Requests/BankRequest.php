<?php

namespace App\Http\Requests;

use App\Models\Bank;
use Illuminate\Foundation\Http\FormRequest;
use Supra\Services\Payment\FlutterwaveInterface;

class BankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        //TODO do verification
        $rules = [];

        $rules['bank'] = ['required', 'exists:banks,id'];
        $rules['account_number'] = ['required', 'numeric'];

        return $rules;
    }
}
