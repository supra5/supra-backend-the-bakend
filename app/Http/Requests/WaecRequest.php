<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Log;
use Supra\Services\Payment\BaxipayInterface;

class WaecRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(BaxipayInterface $baxipay)
    {
        try {
            $billers = $baxipay->fetchPinBundles('waec')['data'];
        } catch (RequestException $e) {
            abort(500, $e->response->message);
        }

        $rules = [
            'pinValue' => ['required', function ($attribute, $value, $fail) use ($billers) {

                if (empty(collect($billers)->firstWhere('amount', $value))) {
                    $fail('Invalid ' . $attribute);
                }
            }],
            'numberOfPins' => ['required', 'integer', 'min:1', function ($attribute, $value, $fail) use ($billers) {
                $biller = collect($billers)->firstWhere('amount', $this->post('postValue'));
                if (!empty($biller) && $value > $biller['available']) {
                    $fail('Not enough amount available. Enter a lower amount');
                }
            }]
        ];
        return $rules;
    }
}
