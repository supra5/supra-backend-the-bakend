<?php

namespace App\Listeners;

use App\Events\ClientUpgradedToMerchant;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyMerchantOnUpgrade
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClientUpgradedToMerchant  $event
     * @return void
     */
    public function handle(ClientUpgradedToMerchant $event)
    {
        //
    }
}
