<?php

namespace App\Listeners;


use App\Events\MerchantLinkUsed;
use App\Mail\NotifyMerchantOnPurchaseMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class NotifyMerchantOnPurchase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillingPurchased  $event
     * @return void
     */
    public function handle(MerchantLinkUsed $event)
    {
        //
        Mail::to($event->merchant)->send(new NotifyMerchantOnPurchaseMail($event->merchant));
    }
}
