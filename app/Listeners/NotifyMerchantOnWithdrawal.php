<?php

namespace App\Listeners;

use App\Events\MerchantRequestWithdrawal;
use App\Mail\MerchantWithdrawalMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class NotifyMerchantOnWithdrawal
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MerchantRequestWithdrawal  $event
     * @return void
     */
    public function handle(MerchantRequestWithdrawal $event)
    {
        //
        Mail::to(
            $event->merchant
        )
            ->send(
                new MerchantWithdrawalMail(
                    $event->merchant,
                    $event->amount
                )
            );
    }
}
