<?php

namespace App\Listeners;


use App\Mail\BillingPurchasedMail;
use App\Events\BillingPurchased;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class NotifyUserOnPurchase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillingPurchased  $event
     * @return void
     */
    public function handle(BillingPurchased $event)
    {
        //
        Mail::to($event->transaction->user)->send(new BillingPurchasedMail($event->transaction));
    }
}
