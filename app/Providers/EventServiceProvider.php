<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        \App\Events\BillingPurchased::class => [
            \App\Listeners\NotifyUserOnPurchase::class,
        ],
        \App\Events\MerchantLinkUsed::class => [
            \App\Listeners\NotifyMerchantOnPurchase::class
        ],
        \App\Events\ClientUpgradedToMerchant::class => [
            \App\Listeners\NotifyMerchantOnUpgrade::class
        ],
        \App\Events\MerchantRequestWithdrawal::class => [
            \App\Listeners\NotifyMerchantOnWithdrawal::class
        ],

    ];



    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
