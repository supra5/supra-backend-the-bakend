<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Supra\Services\AuthService;
use Supra\Services\AuthServiceInterface;
use Supra\Services\Payment\Baxipay;
use Supra\Services\Payment\BaxipayInterface;
use Supra\Services\Payment\BillingService;
use Supra\Services\Payment\BillingServiceInterface;
use Supra\Services\Payment\Flutterwave;
use Supra\Services\Payment\FlutterwaveInterface;

class SupraServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->singleton(Flutterwave::class, function ($app) {
            return new Flutterwave(
                config('flutterwave.api_url'),
                config('flutterwave.public_key'),
                config('flutterwave.secret_key')
            );
        });
        $this->app->singleton(Baxipay::class, function ($app) {
            return new Baxipay(
                config('baxipay.api_url'),
                config('baxipay.api_key'),
                config('baxipay.user_secret'),
                config('baxipay.username')
            );
        });


        $this->app->bind(AuthServiceInterface::class, AuthService::class);
        $this->app->bind(FlutterwaveInterface::class, Flutterwave::class);
        $this->app->bind(BillingServiceInterface::class, BillingService::class);
        $this->app->bind(BaxipayInterface::class, Baxipay::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
