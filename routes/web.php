<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\PaymentController;
use App\Mail\BillingPurchasedMail;
use App\Models\Transaction;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/payment/airtime-callback', [PaymentController::class, 'handleAirtimeCallback']);

Route::get('/payment/data-callback', [PaymentController::class, 'handleDataCallback']);

Route::get('/payment/cable-callback', [PaymentController::class, 'handleCableCallback']);

Route::get('/payment/upgrade-callback', [PaymentController::class, 'handleUpgradeCallback']);

Route::get('/payment/waec-callback', [PaymentController::class, 'handleWaecCallback']);

Route::get('/reset-password/{token}', [ForgotPasswordController::class, 'redirectResetToFrontend'])->name('password.reset');

Route::get('/mailable', function () {
    return new BillingPurchasedMail(Transaction::find(1));
});
