<?php

use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\BillersController;
use App\Http\Controllers\BillingController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\TransactionsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;


Route::middleware('guest:sanctum')->group(function () {
    Route::post('/login', [LoginController::class, 'login']);
    Route::post('/forgot-password', [ForgotPasswordController::class, 'requestResetLink']);
    Route::post('/reset-password', [ForgotPasswordController::class, 'resetPassword']);
    Route::post('/register', [RegisterController::class, 'register']);
});


Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', [UserController::class, 'user']);
    Route::post('/update-profile', [UserController::class, 'updateProfile']);
    Route::post('/change-password', [UserController::class, 'changePassword']);
    Route::post('/logout', [UserController::class, 'logout']);
    Route::post('/buy-airtime', [BillingController::class, 'buyAirtime']);
    Route::post('/buy-data', [BillingController::class, 'buyData']);
    Route::post('/buy-cable', [BillingController::class, 'payCable']);
    Route::post('/buy-power', [BillingController::class, 'buyPower']);
    Route::post('/buy-waec', [BillingController::class, 'buyWaec']);
    Route::get('/my-transactions', [TransactionsController::class, 'getMyTransactions']);
    Route::get('/my-limited-transactions', [TransactionsController::class, 'getMyLimitedTransactions']);
    Route::post('/initiate-upgrade', [BillingController::class, 'initiateUpgrade']);
    Route::get('/all-transactions', [TransactionsController::class, 'getAllTransactions']);
    Route::get('/limited-transactions', [TransactionsController::class, 'limitedTransactions']);
    Route::get('/total-transactions', [TransactionsController::class, 'getTotalTransactions']);
    Route::post('/bulk-airtime-upload', [BillingController::class, 'buyBulkAirtime']);
    Route::post('/generate-link', [UserController::class, 'generateLink']);
    Route::get('/get-merchant-settings', [SettingsController::class, 'getMerchantSettings']);
    Route::post('/update-merchant-settings', [SettingsController::class, 'updateMerchantSettings']);
    Route::get('/get-all-banks', [TransactionsController::class, 'getAllBanks']);
    Route::post('/update-bank-settings', [UserController::class, 'updateBankSetting']);
    Route::post('/withdraw-earning', [UserController::class, 'withdrawEarning']);
    Route::post('/delete-saved-card', [UserController::class, 'deleteSavedCard']);

    Route::prefix('/users')->group(function () {
        //clients
        Route::get('/get-clients', [UsersController::class, 'getClients']);
        Route::post('/add-client', [UsersController::class, 'addClient']);
        Route::post('/delete-client', [UsersController::class, 'deleteClient']);
        Route::get('/count-clients', [UsersController::class, 'countClients']);

        //merchants
        Route::get('/get-merchants', [UsersController::class, 'getMerchants']);
        Route::post('/add-merchant', [UsersController::class, 'addMerchant']);
        Route::post('/delete-merchant', [UsersController::class, 'deleteMerchant']);
        Route::get('/count-merchants', [UsersController::class, 'countMerchants']);

        //Admins
        Route::get('/get-admins', [UsersController::class, 'getAdmins']);
        Route::post('/add-admin', [UsersController::class, 'addAdmin']);
        Route::post('/delete-admin', [UsersController::class, 'deleteAdmin']);
    });
});


Route::prefix('billers')->group(function () {
    Route::get('airtime', [BillersController::class, 'getAirtimeBillers']);
    Route::get('data', [BillersController::class, 'getDataBundleBillers']);
    Route::get('cable', [BillersController::class, 'getCableBillers']);
    Route::get('power', [BillersController::class, 'getPowerBillers']);
    Route::get('waec', [BillersController::class, 'getWaecBillers']);
});
