<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        DB::table('users')->insert([
            [
                'role_id' => 1,
                'name' => 'Precious Ibeagi',
                'email' => 'preciousaang@gmail.com',
                'phone' => '08097218247',
                'password' => bcrypt('albert'),
                'email_verified_at' => now()
            ],
            [
                'role_id' => 2,
                'name' => 'Precious One',
                'email' => 'preciousone@mail.com',
                'phone' => '08097218237',
                'password' => bcrypt('albert'),
                'email_verified_at' => now()
            ],
            [
                'role_id' => 3,
                'name' => 'James John',
                'email' => 'jamesjohn@mail.com',
                'phone' => '08097218532',
                'password' => bcrypt('albert'),
                'email_verified_at' => now()
            ],
        ]);
    }
}
