<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billers', function (Blueprint $table) {
            $table->id();
            $table->string('biller_code');
            $table->string('name');
            $table->float('default_commission')->nullable();
            $table->string('date_added');
            $table->string('country');
            $table->boolean('is_airtime');
            $table->string("biller_name");
            $table->string("item_code");
            $table->string('short_name');
            $table->float('fee')->default(0);
            $table->boolean('commission_on_fee')->nullable();
            $table->string('label_name');
            $table->float('amount')->default(0);
            $table->string('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billers');
    }
}
