<?php

return [
    'api_url' => env('BAXIPAY_API_URL'),
    'api_key' => env('BAXIPAY_API_KEY'),
    'user_secret' => env('BAXIPAY_USER_SECRET'),
    'username' => env('BAXIPAY_USERNAME'),
];
