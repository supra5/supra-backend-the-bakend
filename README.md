# Supra Backend Documentation - (Backend)

## Steps to install

-   Install composer and in your terminal run

```
composer install
```

-   Copy and rename the `.env.example` file and rename it to `.env` , then enter the suitable configuration values

-   In your terminal run

```
php artisan migrate
```

-   Then in your cpanel settings set the root folder the the `public` folder
