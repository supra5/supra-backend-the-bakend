<html>

<head>
    <title>Supra Mail</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        body{
            font-family: sans;
        }
        h4 {
            text-align: center;
        }

        #main {
            width: 50%;
            margin: 30px auto;
        }
        table, tr, td, th{
            border: 1px solid grey;

            color: grey;
        }
        table{
            width: 100%;
        }
        header{
            width: 100%;

            background-color: purple;
            color: white;
            margin: 0;
            height: 50px;
            padding-top: 15px;
        }
    </style>
</head>

<body>
    <header>
    <h4>Supra</h4>
    </header>
    <div id="main">
        Congratulations, {{$merchant->name}}
        <br/><br/>
        You have successfully withdrawn &#8358;{{$amount}}.
    </div>
</body>

</html>
