<html>

<head>
    <title>Supra Mail</title>
    <style>
        *{
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        body{
            font-family: sans;
        }
        h4 {
            text-align: center;
        }

        #main {
            width: 50%;
            margin: 30px auto;
        }
        table, tr, td, th{
            border: 1px solid grey;

            color: grey;
        }
        table{
            width: 100%;
        }
        header{
            width: 100%;

            background-color: purple;
            color: white;
            margin: 0;
            height: 50px;
            padding-top: 15px;
        }
    </style>
</head>

<body>
    <header>
    <h4>Billing Purchased Successful</h4>
    </header>
    <div id="main">
        You have successfully purchased a biller product from us.
        <br/><br/>
        <table>
            <thead>
                <tr>
                    <th>Biller Name</th>
                    @if($transaction->customer)<th>Customer Number</th> @endif
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th>{{$transaction->biller_name}}</th>
                    @if($transaction->customer)<th>{{$transaction->customer}}</th>@endif
                    <th>{{number_format($transaction->amount)}}</th>
                </tr>
            </tbody>
        </table>
    </div>
</body>

</html>
